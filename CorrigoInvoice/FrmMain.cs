﻿using System;
using System.Data;
using System.Windows.Forms;
using System.IO;
using System.Net;

namespace CorrigoInvoice
{
    public partial class FrmMain : Form
    {
        

        string fyleIn = string.Empty;
        string[] columns = null;
        char separator = '|';
        DataTable dtInvoices = new DataTable("Invoices");
        string logFyle = string.Empty;
        string ftpURL = Properties.Settings.Default.aUrl,       // "ftp://ftp.corrigo.com",             //Host URL or address of the FTP server
            userName = Properties.Settings.Default.aUser,        // "FTP_ZAX",                 //User Name of the FTP server
            password = Properties.Settings.Default.aWord,        // "Z4x77cN0w",              //Password of the FTP server
            ftpDirectory = Properties.Settings.Default.aFtpFldr,     // "",          /The directory in FTP server where the files are present
            fileName = string.Empty,
            localFldr = $@"{Directory.GetCurrentDirectory()}/{Properties.Settings.Default.aLocalFldr}";              //File name, which one will be downloaded

        //enum corrigoCols
        //{
        //    LineItemType,
        //    WorkOrderNumber,
        //    WorkOrderCompletedDate,
        //    VendorNumber,
        //    PONumber,
        //    Description,
        //    Qty,
        //    CostRate,
        //    SubTotal,
        //    Total,
        //    InvoiceDate,
        //    InvoiceNumber,
        //    AccountNumber,
        //    RestaurantNumber
        //}

        public FrmMain()
        {
            InitializeComponent();
        }
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            
           

            DateTime dte = DateTime.Now.AddDays(-1);
            logFyle = $"{localFldr}//Corrigo.log";
            try
            {
                string fyleLaw = $@"{Properties.Settings.Default.aLawsonFOlder}QP620IN";
                if (File.Exists(fyleLaw))
                {
                    File.Delete(fyleLaw);
                }
                if (!Directory.Exists(localFldr))
                {
                    Directory.CreateDirectory(localFldr);
                }
                fileName = $"Ok-To-Pay_Corrigo{dte.Month:00}{dte.Day:00}{dte.Year}.csv";
                // fileName = $"Ok-To-Pay_Corrigo05022017.csv";
                if (DownloadFile(ftpURL, userName, password, ftpDirectory, fileName, localFldr))
                {
                    createLawsonFile();
                    File.AppendAllText(logFyle, $"{DateTime.Now} ---File:{fileName} processed" + Environment.NewLine);
                    // send email with file attached
                    using (CodeEmail clsEmail = new CodeEmail())
                    {
                        clsEmail.AttachementPath = $@"{Properties.Settings.Default.aLawsonFOlder}QP620IN";
                        clsEmail.FyleProcessed = fileName;
                        clsEmail.SendEmail();
                    }
                }
                else
                {
                    File.Create(fyleLaw);
                }
            }
            catch (Exception ex)
            {
                File.AppendAllText(logFyle, $"{DateTime.Now} ---Error:{ex.Message}" + Environment.NewLine);
            }
            Application.Exit();
        }
    
        private void cmdBrowse_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlgFyleIn = new OpenFileDialog();
            dlgFyleIn.RestoreDirectory = true;
            dlgFyleIn.Filter = "All files (*.*)|*.*";
            if (dlgFyleIn.ShowDialog() == DialogResult.OK)
            {
                fyleIn = dlgFyleIn.FileName.Trim();
                tbxFile.Text = fyleIn;
            }
        }

        private void cmdCreateLawsonFile_Click(object sender, EventArgs e)
        {
            createLawsonFile();
        }
        private void createLawsonFile()
        { 
            string fyleLawson = $"{localFldr}//CorrigoInvoices.csv",
                cVendor = string.Empty,
                cInvoice = string.Empty,
                cInvDate = string.Empty,
                cDesc = string.Empty,
                cAcctUnit = string.Empty,
                cDisAcct = string.Empty;
            decimal invLineAmt = 0,
                invTotAmt = 0;
            try
            {
                if (tbxFile.Text.Trim().Length == 0)
                {
                    MessageBox.Show("Please select a file to import.", "Entry Required", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                fyleIn = tbxFile.Text;

                if (File.Exists(fyleLawson))
                {
                    File.Delete(fyleLawson);
                }
               // File.AppendAllText(fyleLawson, $"VENDOR,INVOICE,INVOICE-DTE,DESCRIPTION,AMOUNT,DIS-ACCT-UNIT,DIS-ACCOUNT{Environment.NewLine}");

                string[] lynes = File.ReadAllLines(fyleIn);
                if (lynes.GetUpperBound(0) > 0)
                {
                    columns = lynes[0].Split(new char[] { separator });
                    foreach (string col in columns)
                    {
                        dtInvoices.Columns.Add(col.ToString().Replace(" ","_"));
                    }

                    int cntLines = 1;
                    while (cntLines <= lynes.GetUpperBound(0))
                    {
                        DataRow dRow = dtInvoices.NewRow();
                        string[] datas = lynes[cntLines].Split(new char[] { separator });
                        for (int cntCol = 0; cntCol <= datas.GetUpperBound(0); cntCol++)
                        {
                            dRow[cntCol] = datas[cntCol];
                        }
                        dtInvoices.Rows.Add(dRow);
                        cntLines++;
                    }
                }
                DataView dView = new DataView(dtInvoices);
                DataTable dtInvNumbers = dView.ToTable(true, "Invoice_Number");
                int cntDetRows = 0;
                foreach(DataRow vInvNbr in dtInvNumbers.Rows)
                {
                    DataRow[] drInvRows = dView.Table.Select($"Invoice_Number='{vInvNbr[0]}'");
                    if (drInvRows.GetUpperBound(0) > 0)
                    {
                        cntDetRows = 1;
                        foreach (DataRow detRow in drInvRows)
                        {
                            cVendor = detRow["VendorNumber"].ToString();
                            cInvoice = $@"{vInvNbr[0]}";        // -{cntDetRows}";
                            cInvDate = fixDate(detRow["Invoice_Date"].ToString());
                            cDesc = detRow["Description"].ToString();
                            cAcctUnit = getInforAU(detRow["Restaurant_Number"].ToString());
                            cDisAcct = detRow["Account_Number"].ToString();
                            invLineAmt = 0;
                            invTotAmt = 0;
                            //foreach (DataRow dRow in drInvRows)
                            //{
                                invTotAmt = decimal.TryParse(detRow["SubTotal"].ToString(), out invLineAmt) ? invLineAmt : 0;
                            //}
                            File.AppendAllText(fyleLawson, $"{cVendor}{separator}{cInvoice}{separator}{cntDetRows}{separator}{cInvDate}{separator}{cDesc}{separator}{invTotAmt:0.00}{separator}{cAcctUnit}{separator}{cDisAcct}{Environment.NewLine}");
                            cntDetRows++;
                        }
                    }
                }
                File.Copy(fyleLawson, $@"{Properties.Settings.Default.aLawsonFOlder}QP620IN", true);
                string archivefiletest = "QP620IN_" + DateTime.Now.ToString() + ".csv";
                archivefiletest = archivefiletest.Replace("/", "_");
                archivefiletest = archivefiletest.Replace(":", "_");
                archivefiletest = archivefiletest.Replace(" ", "_");

                File.Copy(fyleLawson, $@"{Properties.Settings.Default.ArchiveFolder}" + archivefiletest, true);
            }
            catch(Exception ex)
            {
                File.AppendAllText(logFyle, $"{DateTime.Now} ---Error:{ex.Message}" + Environment.NewLine);
                //MessageBox.Show(ex.Message);
            }
            Application.Exit();
        }
        public bool DownloadFile(string ftpURL, string userName, string password, string ftpDirectory, string fyleName, string localFldr)
        {
            bool rslt = false;
            string logFyle = $"{localFldr}//Corrigo.log";
            WebClient request = new WebClient();
            request.Credentials = new NetworkCredential(userName, password);
            try
            {
                byte[] newFileData = request.DownloadData(ftpURL + "//"+ fyleName);
                string fileString = System.Text.Encoding.UTF8.GetString(newFileData),
                    fyleOut = $@"{localFldr}\{fyleName}";
                if (File.Exists(fyleOut))
                {
                    File.Delete(fyleOut);
                }
                File.WriteAllText(fyleOut, fileString);
                rslt = true;
                tbxFile.Text = fyleOut;
            }
            catch (Exception ex)
            {
                File.AppendAllText(logFyle, $"{DateTime.Now} ---Server:{ftpURL} ---Results:{ex.Message}" + Environment.NewLine);
                //MessageBox.Show(ex.Message);
            }
            return rslt;
        }
         private string fixDate(string inDte)
        {
            string rslt = string.Empty;
            string[] datas = new string[3];
            if(inDte.Contains("/"))
            {
                datas = inDte.Split('/');
            }
            else
            {
                datas = inDte.Split('-');
            }
            rslt = $@"{datas[0].PadLeft(2,'0')}/{datas[1].PadLeft(2, '0')}/{datas[2].PadLeft(4, '0')}";
            return rslt;
        }
        private string getInforAU(string store)
        {
            string fourAcctUnit = string.Empty;
            if (store.Trim().Length > 0)
            {
                string storeID = store.PadLeft(5, '0');
                fourAcctUnit = storeID.Substring(3, 2);
                string tmp = fourAcctUnit.Substring(0, 1);
                switch (tmp)
                {
                    case "A":
                        fourAcctUnit = string.Format("1{0}{1}00", "10", fourAcctUnit.Substring(1));
                        break;
                    case "B":
                        fourAcctUnit = string.Format("1{0}{1}00", "11", fourAcctUnit.Substring(1));
                        break;
                    case "C":
                        fourAcctUnit = string.Format("1{0}{1}00", "12", fourAcctUnit.Substring(1));
                        break;
                    case "D":
                        fourAcctUnit = string.Format("1{0}{1}00", "13", fourAcctUnit.Substring(1));
                        break;
                    case "E":
                        fourAcctUnit = string.Format("1{0}{1}00", "14", fourAcctUnit.Substring(1));
                        break;
                    case "F":
                        fourAcctUnit = string.Format("1{0}{1}00", "15", fourAcctUnit.Substring(1));
                        break;
                    default:
                        fourAcctUnit = string.Format("10{0}00", fourAcctUnit);
                        break;
                }
            }
            return fourAcctUnit;
        }

    }
}

﻿using System;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;

namespace CorrigoInvoice
{
    class CodeEmail : IDisposable
    {
        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
        public string FyleProcessed
        { set; get; }
        public string AttachementPath
        { set; get; }
        /// <summary>Send the email message.</summary>
        /// <returns>True if sent, false if error encountered.</returns>
        /// 
        public bool SendEmail()
        {
            bool rslt = true;
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();
                message.Subject = "Invoices from Corrigo";
                StringBuilder sbMessage = new StringBuilder();
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient("email.zaxbys.com");
                StringBuilder sbRecEmail = new StringBuilder();
                //message.To.Add("dcarter@zaxbys.com");
                message.To.Add("wthomas@zaxbys.com");
                message.To.Add("ayoungblood@zaxbys.com");
                message.To.Add("tjacquet@zaxbys.com");
                message.From = new System.Net.Mail.MailAddress("ablashaw@zaxbys.com", "ZAX Sales");
                message.CC.Add("pjohnson@zaxbys.com");
                message.CC.Add("ablashaw@zaxbys.com");
                //sbMessage.Append("-----TEST-----TEST-----TEST-----TEST-----TEST-----"+Environment.NewLine);
                //sbMessage.Append("-----Do not pass GO. Do not load any files.-----" + Environment.NewLine);
                sbMessage.Append($@"The Corrigo invoice file {FyleProcessed} has been loaded to {AttachementPath}" + Environment.NewLine);
                sbMessage.Append(Environment.NewLine);
                //sbMessage.Append("Just in case this file did not load.");
                //sbMessage.Append(Environment.NewLine);
                message.Priority = MailPriority.High;
                message.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                message.Body = sbMessage.ToString();
                                                            
                //System.Net.Mail.Attachment attachedFile;
                //attachedFile = new System.Net.Mail.Attachment(AttachementPath, MediaTypeNames.Application.Octet);
                //message.Attachments.Add(attachedFile);
                smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
                smtp.UseDefaultCredentials = false;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.EnableSsl = false;
                smtp.Send(message);

                smtp.Dispose();
                message.Dispose();
            }
            catch (Exception ex)
            {
                rslt = false;
            }
            return rslt;
        }

    }
}
